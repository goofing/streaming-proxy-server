# Get the GCC preinstalled image from Docker Hub
FROM gcc:9.3.0

# Copy the current folder which contains C++ source code to the Docker image under /usr/src
COPY . /usr/src/app
ENV PROJECT_SOURCE_DIR "/usr/src/app"
# Specify the working directory
WORKDIR /usr/src/app

RUN wget https://cmake.org/files/v3.17/cmake-3.17.0-Linux-x86_64.tar.gz; tar -zxvf cmake-3.17.0-Linux-x86_64.tar.gz
# Use GCC to compile the Test.cpp source file
RUN set -ex;                \
    cd /usr/src/app;        \
    ./cmake-3.17.0-Linux-x86_64/bin/cmake . ; make;

RUN ls -al

# Run the program output from the previous step
CMD ["./streaming_proxy_server"]